# Product Management

- Ensure Python 3 is installed
- Create virtual env:
```
python3 -m venv /path/to/venv
```
- Activate virtualenv:
```
source /path/to/venv/bin/activate
```
- Install requirements:
```
pip install -r requirements.txt
```
- Run migrations:
```
python manage.py migrate
```
- Run development server:
```
python manage.py runserver
```
- Heroku Hosted Version
```
https://productmanagedemo.herokuapp.com
```
- Postman dump
```
https://documenter.getpostman.com/view/12720185/Tz5m8fKo
```