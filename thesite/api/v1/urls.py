from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from products.api.v1 import urls as product_urls


urlpatterns = [
    url(r'products/', include(product_urls)),
]
