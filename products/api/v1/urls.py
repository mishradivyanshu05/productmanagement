from django.urls import path
from products.api.v1 import views

urlpatterns = [
    path('category_list/', views.ListCategoryAPIView.as_view(), name='category_list'),
    path('sub_category/<int:category>/', views.ListSubCategoryAPIView.as_view(), name='sub_category_list'),
    path('', views.ListCreateProductAPIView.as_view(), name='list_create_product'),
]