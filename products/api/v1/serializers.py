from rest_framework import serializers
from products.models import Product, Category, SubCategory
from .serializer_utils import DynamicFieldsModelSerializer


class ProductSerializer(serializers.ModelSerializer):
    category_name = serializers.ReadOnlyField()
    sub_category_name = serializers.ReadOnlyField()

    class Meta:
        model = Product
        fields = '__all__'


class SubCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = SubCategory
        fields = "__all__"


# class CategorySerializer(DynamicFieldsModelSerializer):
#     products = serializers.SerializerMethodField()
#     subcategory = serializers.SerializerMethodField()
#
#     class Meta:
#         model = Category
#         fields = ['id', 'name', 'subcategory', 'products']
#
#     def get_products(self, obj):
#         category_id = self.context['view'].kwargs['pk']
#         return Products.objects.filter(sub_categories__categories__pk=category_id)\
#                                .values('id', 'name', 'sub_categories__name')
#
#     def get_subcategory(self, obj):
#         return obj.subcategory.all().values('id', 'name')


class CategoryListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'
