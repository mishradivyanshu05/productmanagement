from django_filters import rest_framework as filters

from products.models import Product


class ProductFilter(filters.FilterSet):
    category = filters.CharFilter(field_name='sub_categories__category')

    class Meta:
        model = Product
        fields = ('sub_categories', 'category')

