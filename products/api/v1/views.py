from rest_framework import generics
from django_filters import rest_framework as filters

from products.models import Product, Category, SubCategory
from .serializers import ProductSerializer, SubCategorySerializer,\
                         CategoryListSerializer
from .filters import ProductFilter


class ListCreateProductAPIView(generics.ListCreateAPIView):
    """ ADD and List product API """

    serializer_class = ProductSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ProductFilter
    queryset = Product.objects.all()


class ListCategoryAPIView(generics.ListAPIView):
    """List of categories."""

    queryset = Category.objects.all()
    serializer_class = CategoryListSerializer


class ListSubCategoryAPIView(generics.ListAPIView):
    """List of subcategories for a category"""

    serializer_class = SubCategorySerializer

    def get_queryset(self):
        return SubCategory.objects.filter(category=self.kwargs.get('category'))
#
#
# class CategoryProductRetrieveAPIView(generics.RetrieveAPIView):
#     """ Products or Subcategory list for a category."""
#
#     queryset = Categories.objects.all()
#     serializer_class = CategorySerializer
#
#
# class SubCategoryProductRetrieveAPIView(generics.RetrieveAPIView):
#     """ List products for sub-categories. """
#
#     serializer_class = SubCategorySerializer
#     queryset = SubCategories.objects.all().prefetch_related('products')
