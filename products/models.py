from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class SubCategory(models.Model):
    name = models.CharField(max_length=200)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='subcategory')

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=200)
    sub_categories = models.ForeignKey(
        SubCategory, on_delete=models.CASCADE, related_name='product_detail'
    )

    def __str__(self):
        return self.name

    @property
    def sub_category_name(self):
        return self.sub_categories.name

    @property
    def category_name(self):
        return self.sub_categories.category.name
